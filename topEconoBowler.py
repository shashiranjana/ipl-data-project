'''
calculating top economy of bowler
'''
import csv
import operator
from matplotlib import pyplot as plt


def store_match_id(matches_file_path):
    '''
    storing match id value of 2015
    '''
    with open(matches_file_path, newline='') as matches_file:
        matches_reader = csv.DictReader(matches_file)
        season_with_id = {}
        for match in matches_reader:
            season = match['season']
            match_id = match['id']
            if int(season) == 2015:
                if season in season_with_id:
                    season_with_id[season] = season_with_id[season]+" " + match_id
                else:
                    season_with_id[season] = match_id
    list_of_match_id = season_with_id['2015'].split()
    return list_of_match_id

def cal_economy_bowler(deliveries_file_path, list_of_match_id):
    '''
    calculate economy of bowler
    '''
    with open(deliveries_file_path, newline='') as deliveries_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        total_runs_conceded = {}
        bowler_wise_ball = {}
        economy_of_bowlers = {}
        for delivery in deliveries_reader:
            is_super_over = int(delivery['is_super_over'])
            current_match_id = delivery['match_id']
            if is_super_over == 0:
                if current_match_id in list_of_match_id:
                    wide_runs = delivery['wide_runs']
                    no_ball_runs = delivery['noball_runs']
                    batsman_runs = delivery['batsman_runs']
                    bowlers_name = delivery['bowler']
                    total_runs = int(wide_runs)+int(no_ball_runs)+int(batsman_runs)
                    if bowlers_name in total_runs_conceded:
                        total_runs_conceded[bowlers_name] = total_runs + int(
                            total_runs_conceded[bowlers_name])
                        if int(wide_runs) == 0 and int(no_ball_runs) == 0:
                            bowler_wise_ball[bowlers_name] += 1
                            economy_of_bowlers[bowlers_name] = (
                                int(total_runs_conceded[bowlers_name])/int(
                                    bowler_wise_ball[bowlers_name]))*6
                        else:
                            economy_of_bowlers[bowlers_name] = (
                                int(total_runs_conceded[bowlers_name])/int(
                                    bowler_wise_ball[bowlers_name]))*6
                    else:
                        total_runs_conceded[bowlers_name] = total_runs
                        if int(wide_runs) == 0 and int(no_ball_runs) == 0:
                            bowler_wise_ball[bowlers_name] = 1
                            economy_of_bowlers[bowlers_name] = (
                                int(total_runs_conceded[bowlers_name])/int(
                                    bowler_wise_ball[bowlers_name]))*6
                        else:
                            bowler_wise_ball[bowlers_name] = 0
        sorted_x = sorted(economy_of_bowlers.items(), key=operator.itemgetter(1))
        return sorted_x[:10]

def plot(economy_reate):
    '''
    plotting of economy rate
    '''
    bowler_name = []
    economy_of_bowler = []
    for value in economy_reate:
        bowler_name.append(value[0])
        economy_of_bowler.append(value[1])
    print(economy_of_bowler)
    plt.bar(bowler_name, economy_of_bowler)
    plt.xticks(rotation=20, fontsize='x-small')
    plt.xlabel("Bowler name    ----------------->")
    plt.ylabel("Economy of bowler  ----------------->")
    plt.show()

def execute():
    '''
    executing file
    '''
    list_of_match_id = store_match_id(
        "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\ipl\\matches.csv")
    sorted_val = cal_economy_bowler(
        "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\ipl\\deliveries.csv",
        list_of_match_id)
    plot(sorted_val)

execute()

