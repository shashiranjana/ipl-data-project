'''
calculating no of extra runs per team
'''
import csv
from matplotlib import pyplot as plt

def extract(matches_file_path, deliveries_file_path):
    '''
    extraction of extractions data
    '''
    with open(deliveries_file_path, newline='') as deliveries_file, open(
        matches_file_path, newline='') as matches_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        matches_reader = csv.DictReader(matches_file)
        total_match_id = {}
        for match_id in matches_reader:
            if match_id['season'] in total_match_id:
                total_match_id[match_id['season']] = match_id[
                    'id']+","+total_match_id[match_id['season']]
            else:
                total_match_id[match_id['season']] = match_id['id']
        total_id_in_2016 = total_match_id['2016'].split(",")
        extra_run_conceded_by_team = {}
        for deliveries_id in deliveries_reader:
            if deliveries_id['match_id'] in total_id_in_2016:
                if deliveries_id['bowling_team'] in extra_run_conceded_by_team:
                    extra_run_conceded_by_team[deliveries_id['bowling_team']] = int(
                        deliveries_id['extra_runs']) + int(
                            extra_run_conceded_by_team[deliveries_id['bowling_team']])
                else:
                    extra_run_conceded_by_team[deliveries_id['bowling_team']] = int(
                        deliveries_id['extra_runs'])
        return extra_run_conceded_by_team

def plot(extra_runs_per_team):
    '''
    plotting data
    '''
    print(extra_runs_per_team)
    list_of_team = []
    list_of_no_of_extra_runs = []
    for year, no_of_count in extra_runs_per_team.items():
        list_of_team.append(year)
        list_of_no_of_extra_runs.append(no_of_count)
    plt.bar(list_of_team, list_of_no_of_extra_runs)
    plt.xticks(rotation=20, fontsize='x-small')
    plt.xlabel("Team  Name    ----------------->")
    plt.ylabel("Extra Runs Conceded   ----------------->")
    plt.show()

def execute():
    '''
    for execution
    '''
    extra_runs_per_team = extract(
        "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\ipl\\matches.csv", "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\ipl\\deliveries.csv")
    plot(extra_runs_per_team)

execute()
