'''
calculating top batting pitch and bowling pitch
'''

import csv
import operator
from matplotlib import pyplot as plt

def extract(matches_file_path, deliveries_file_path):
    '''
    extracting data for bowling and batting pitch
    '''
    with open(deliveries_file_path, newline='') as deliveries_file, open(
        matches_file_path, newline='') as matches_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        matches_reader = csv.DictReader(matches_file)
        matchId_and_runs = {}
        for delivery in deliveries_reader:
            if delivery['match_id'] in matchId_and_runs:
                matchId_and_runs[delivery['match_id']] = int(
                    delivery['total_runs']) + int(matchId_and_runs[delivery['match_id']])
            else:
                matchId_and_runs[delivery['match_id']] = delivery['total_runs']
        venue_matches = {}
        total_matches_in_venue = {}
        average_score_in_venue = {}
        for match in matches_reader:
            match_id = match['id']
            runs_scored = matchId_and_runs[match_id]
            if match['venue'] in venue_matches:
                venue_matches[match['venue']] = int(runs_scored) + int(
                    venue_matches[match['venue']])
                total_matches_in_venue[match['venue']] += 1
                average_score_in_venue[match['venue']] = int(
                    venue_matches[match['venue']])/int(total_matches_in_venue[match['venue']])
            else:
                venue_matches[match['venue']] = int(runs_scored)
                total_matches_in_venue[match['venue']] = 1
                average_score_in_venue[match['venue']] = int(
                    venue_matches[match['venue']])/int(
                        total_matches_in_venue[match['venue']])
        sorted_venue_and_runs = sorted(average_score_in_venue.items(),
                                       key=operator.itemgetter(1))
        '''top 5 bowling pitch and battings pitch '''
        top_bowling_pitch = sorted_venue_and_runs[:5]
        top_batting_pitch = sorted_venue_and_runs[-5:]
        return top_bowling_pitch, top_batting_pitch

def plot(top_bowling_pitch, top_batting_pitch):
    '''
    plotting data
    '''
    batting_pitch = []
    avg_run_batting_pitch = []
    bowling_pitch = []
    avg_run_bowling_pitch = []

    for count in range(len(top_batting_pitch)):
        batting_pitch.append(top_batting_pitch[count][0])
        avg_run_batting_pitch.append(top_batting_pitch[count][1])
        bowling_pitch.append(top_bowling_pitch[count][0])
        avg_run_bowling_pitch.append(top_bowling_pitch[count][1])

    plt.subplot(1, 2, 1)
    plt.bar(batting_pitch, avg_run_batting_pitch)
    plt.xticks(rotation=10, fontsize='x-small')
    plt.legend(bbox_to_anchor=(1.05, 1), ncol=5, loc=4, borderaxespad=1)
    plt.ylabel("Average Runs Scored   ----------------->")
    plt.title("Top 5 Batting Pitch")
    plt.subplot(1, 2, 2)
    plt.title("Top 5 Bowling Pitch")
    plt.bar(bowling_pitch, avg_run_bowling_pitch)
    plt.xticks(rotation=20, fontsize='x-small')
    plt.legend(bbox_to_anchor=(1.05, 1), ncol=5, loc=4, borderaxespad=1)
    plt.xlabel("Venue Name    ----------------->")
    plt.ylabel("Average Runs Scored   ----------------->")
    plt.show()



def execute():
    '''
    executing extract and plot methods
    '''
    top_bowling_pitch, top_batting_pitch = extract(
        "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\ipl\\matches.csv", "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\ipl\\deliveries.csv")
    plot(top_bowling_pitch, top_batting_pitch)

execute()
