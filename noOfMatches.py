'''
calculating no of matches per team
'''
import csv
from matplotlib import pyplot as plt


def extract(filename):
    '''
    calculating no. of matches played by team per season
    '''
    with open(filename, newline='') as matches_file:
        matches_reader = csv.DictReader(matches_file)
        matches_per_season = {}
        for match in matches_reader:
            season = match['season']
            if (season in matches_per_season):
                matches_per_season[season] += 1
            else:
                matches_per_season[season] = 1
        print(matches_per_season)
    return matches_per_season

def plot(matches_per_season):
    '''
    plotting data
    '''
    list_of_year = []
    list_of_no_of_counts = []
    for year, no_of_count in matches_per_season.items():
        list_of_year.append(year)
        list_of_no_of_counts.append(no_of_count)
    plt.bar(list_of_year, list_of_no_of_counts)
    plt.xticks(rotation=20, fontsize='x-small')
    plt.xlabel("List of Year    ----------------->")
    plt.ylabel("No. of matches played  ----------------->")
    plt.show()

def execute():
    '''
    executing extract and plot data
    '''
    plot(extract(
        "C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\ipl\\matches.csv"))

execute()
