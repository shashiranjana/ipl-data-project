'''
calculating no of matches won by per team
'''

import csv
import operator
from operator import add
from matplotlib import pyplot as plt

def readAndStoreData(fileName):
    '''
    extracting no of matches won by per team
    '''
    with open(fileName, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        winning_team = {}
        total_teams = []
        for match in reader:
            keyAsYear = match['season']
            valAsWinnerTeam = match['winner']
            if valAsWinnerTeam != '':
                if keyAsYear in winning_team:
                    winning_team[keyAsYear] = valAsWinnerTeam +","+winning_team[keyAsYear]
                else:
                    winning_team[keyAsYear] = valAsWinnerTeam
                if  valAsWinnerTeam not in total_teams:
                    total_teams.append(valAsWinnerTeam)
        total_teams.sort()
        return total_teams, winning_team

def calculate_no_of_win(total_teams, winning_team):
    '''
    calculating no. of win per team wise
    '''
    year_team_value = {}
    for year in winning_team:
        total_win_team = winning_team[year].split(',')
        team_and_count = {}
        total_team_played = []
        for team in total_win_team:
            if team in team_and_count:
                team_and_count[team] += 1
            else:
                team_and_count[team] = 1
                total_team_played.append(team)
        not_played_team = list(set(total_teams)-set(total_team_played))
        val_not_played_by_team = {}
        for team in not_played_team:
            val_not_played_by_team[team] = 0
        team_and_count.update(val_not_played_by_team)
        sorted_team_value = sorted(team_and_count.items(), key=operator.itemgetter(0))
        year_team_value[year] = sorted_team_value
    return year_team_value, total_teams

def plot(year_team_value, total_teams):
    '''
    plotting data
    '''
    x_value = total_teams
    yvalue = list(range(10, 100, 10))
    bottom_result = [0]*14
    for year in year_team_value:
        val_of_season = year_team_value[year]
        value_list = []
        for value in val_of_season:
            value_list.append(value[1])
        plt.bar(x=x_value, height=value_list, label=year, width=0.5, bottom=bottom_result)
        bottom_result_list = list(map(add, bottom_result, value_list))
        bottom_result = bottom_result_list
    plt.legend(bbox_to_anchor=(1.05, 1), ncol=5, loc=4, borderaxespad=1)
    plt.xlabel("Team Name    ----------------->")
    plt.ylabel("Years    ----------------->")
    plt.xticks(rotation=20, fontsize='x-small')
    plt.yticks(yvalue)
    plt.show()

def execute():
    '''
    executing file
    '''
    total_teams, winning_team = readAndStoreData(
        'C:\\Users\\shash\\Videos\\Mountblue\\Project\\IPL Data Project\\ipl\\matches.csv')
    year_team_value, total_teams = calculate_no_of_win(total_teams, winning_team)
    plot(year_team_value, total_teams)

execute()